class Biginteger{
    private s:string;
    constructor(s="0"){
		this.s=s;
		this.removeLeadingZeros();
    }

    public copy():Biginteger{
		return new Biginteger(this.s)
    }

    public add(b:Biginteger):Biginteger{
		if(b.s.length>this.s.length)
		    return b.add(this)

		let s = ""
		let carry = 0
		for(var ai = this.s.length-1, bi = b.s.length-1; bi>=0;ai--,bi--){
		    let digit = parseInt(this.s[ai])+parseInt(b.s[bi])+carry
		    carry = Math.floor(digit/10)
		    digit%=10
		    s=digit+s
		}

		for(;ai>=0;ai--){
		    let digit = parseInt(this.s[ai])+carry
		    carry = Math.floor(digit/10)
		    digit%=10
		    s=digit+s
		}
		if(carry != 0)
		    s=carry+s
		return new Biginteger(s)
    }

    public pow(b:number):Biginteger{
		if(b == 1)
		    return this.copy()
		let squared = this.pow(Math.floor(b/2))
		let res = squared.mult(squared)
		if(b%2!=0){
		    res = res.mult(this)
		}
		return res
    }

    private removeLeadingZeros():Biginteger{
		while (this.s.length>1 && this.s[0]=='0'){
		    this.s = this.s.substring(1)
		}
		return this
    }
    
    public mult(b:Biginteger):Biginteger{
		if(b.s.length>this.s.length)
		    return b.mult(this)

		// b is smaller than this
		let r = new Biginteger()
		for(var ib = b.s.length-1, shift = 0; ib>=0;ib--, shift++){
		    let s = new Array(shift + 1).join('0') // init with shift zeros

		    let carry = 0
		    for(var ia = this.s.length-1; ia>=0;ia--){

			let digit=parseInt(b.s[ib])*parseInt(this.s[ia])+carry
			carry = Math.floor(digit/10)
			digit%=10
			s = digit+s
		    }
		    if(carry != 0)
			s = carry+s

		    r = r.add(new Biginteger(s))
		}
    	return r
    }

    private adjustLength(length:number):Biginteger{
		if(length<this.s.length){
		    console.log("Error: adjusting size down...")
		}
		this.s = new Array(length + 1 - this.s.length).join('0')+this.s
		return this
    }

    public toString = () : string => {
        return "Biginteger ("+this.s+")";
    }
}

let a = new Biginteger("900000")
let b = new Biginteger("1100000")
let c = new Biginteger("100")
let d = new Biginteger("39")
let e = new Biginteger("142")
console.log(c+"+"+e+"="+c.add(e))
console.log(a+"+"+b+"="+a.add(b))
console.log(c+"*"+d+"="+c.mult(d))
let test = new Biginteger("00001000")
let t2 = new Biginteger("00")
console.log(test+"")
console.log(t2+"")
console.log(e+"^7="+e.pow(7))
console.log(e+"^10="+e.pow(10))
