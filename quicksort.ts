interface Sorting<T>{
    sort(array:T[]);
}

class QuickSort<T> implements Sorting<T>{
    public sort(array:T[]){
        this.quicksort(array);
    }
    private quicksort(a: Array<T>, left = 0, right = a.length - 1){
        if(left<right){
        let pivot = right
        let idx = this.partition(a, pivot, left, right)
            this.quicksort(a, left,idx-1)
            this.quicksort(a, idx+1,right) 
        }
        return a;
    }
    private partition(a: Array<T>, pivot:number, left: number, right:number):number{
        let pvalue = a[pivot]
        let part_idx = left

        for(var i = left; i<right;i++){
            if(a[i]<pvalue){
                [a[i], a[part_idx]] = [a[part_idx], a[i]]
                part_idx++;
            }
        }
        [a[right], a[part_idx]] = [a[part_idx], a[right]]
        return part_idx
    }
}

let number_array = [2,8,1,4,6,5,3]
console.log(number_array)
new QuickSort<number>().sort(number_array);
console.log(number_array)

let string_array = ["bb", "ba", "aa","ca","ac","de"]
console.log(string_array)
new QuickSort<string>().sort(string_array);
console.log(string_array)

