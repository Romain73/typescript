interface ICripto{
    cifrar(m:string):string;
    descifrar(c:string):string;
}

class CifradoCesar implements ICripto {
    private n:number;

    constructor(k:number){
        this.n = k;
    }

    private static shiftStr(s:string, n:number):string{
        let out="";
        s = s.replace(/[^a-z]/g, '');
        for (var i = 0; i < s.length; i ++) {
            var ascii = s.charCodeAt(i);
            out+=String.fromCharCode(((ascii - 97 + n + 26) % 26) + 97);
        }
        return out;
    }

    public cifrar(m:string):string{
        return CifradoCesar.shiftStr(m, this.n);
    }
    public descifrar(c:string):string{
        return CifradoCesar.shiftStr(c, -this.n);
    }
}

class OneTimePad implements ICripto {
    private mask:string;

    constructor(mask:string){
        this.mask = mask.replace(/[^a-z]/g, '');
    }

    public cifrar(m:string):string{
        let out="";
        let s = m.replace(/[^a-z]/g, '');

        if(s.length>this.mask.length)
            throw "The mask must be longer than the message to encrypt."
        for (var i = 0; i < s.length; i ++) {
            let c = s.charCodeAt(i)-97;
            let x = this.mask.charCodeAt(i)-97;
            out+=String.fromCharCode((c-x+26)%26 + 97);
        }
        return out;
    }
    public descifrar(c:string):string{
        let out="";
        let s = c.replace(/[^a-z]/g, '');

        if(s.length>this.mask.length)
            throw "The mask must be longer than the message to decrypt."
        for (var i = 0; i < s.length; i ++) {
            let c = s.charCodeAt(i)-97;
            let x = this.mask.charCodeAt(i)-97;
            out+=String.fromCharCode((c+x)%26 + 97);
        }
        return out;
    }
}


let s = "the quick brown fox jumps over the lazy dog";
let algos =[new CifradoCesar(0), new CifradoCesar(2),
            new CifradoCesar(-2),new CifradoCesar(26)
            , new OneTimePad("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
            , new OneTimePad("jklmnopqrstuvwxyzabcdefghijklmnopqr")
            , new OneTimePad("thequickbrownfoxjumpsoverthelazydog")
            ];
for(var i in algos){
    var a = algos[i];
    console.log(a);
    console.log(a.cifrar(s))
    console.log(a.descifrar(a.cifrar(s)))
    console.log("")
}
