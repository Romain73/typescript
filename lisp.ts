enum TokenType {
    NUMBER,
    ID,
    P_OPEN,
    P_CLOSE
}
class Token{
    private t:TokenType;
    private lexema:string;
    constructor(t:TokenType, lexema:string){
        this.t=t;
        this.lexema=lexema;
    }
    public getType():TokenType{
        return this.t;
    }
    public getLexema():string{
        return this.lexema;
    }
}

interface INode{
    execute():any;
}

class NodeFcall implements INode{
    public fnName:NodeID;
    public params:Array<INode>;
    constructor(){
        this.params = new Array<INode>();
    }
    static functions = {
        "+":function(a, b){return a+b},
        "-":function(a, b){return a-b},
        "*":function(a, b){return a*b},
        "/":function(a, b){return a/b},
    }
    execute():any{
        return NodeFcall.functions[this.fnName.execute()](this.params[0].execute(), this.params[1].execute());
    }
}

class NodeID implements INode{
    public lexema:string;
    constructor(lexema:string){
        this.lexema = lexema;
    }
    execute():any{
        return this.lexema;
    }
}

class NodeNumber implements INode{
    execute():any{
        return Number(this.lexema)
    }
    public lexema:string;
    constructor(lexema:string){
        this.lexema = lexema;
    }
}


class LispInterprete {
    
    private tokens:Array<Token>
    private parseTree:INode
    public tokenize(s:string){
        this.tokens = new Array<Token>();
        for(var i = 0; i<s.length;i++){
            if(s[i]=="("){
                this.tokens.push(new Token(TokenType.P_OPEN, "("))
            }else if(s[i] == ")"){
                this.tokens.push(new Token(TokenType.P_CLOSE, ")"))
            }else{
                let rest = s.substring(i);
                let matchNb = rest.match(/^[0-9]+(\\.[0-9]*)?/);
                if(matchNb!=null){
                    this.tokens.push(new Token(TokenType.NUMBER, matchNb[0]))
                    i+=matchNb[0].length-1
                }else{
                    let matchID = rest.match(/^[^\ \)\(]+/);
                    if(matchID!=null){
                        this.tokens.push(new Token(TokenType.ID, matchID[0]))
                        i+=matchID[0].length-1
                    }
                }
            }
        }
        console.log(this.tokens)
    }
    public parse(){
        this.parseTree = this.parseIntern();
    }

    private parseIntern():INode{
        let t = this.tokens.shift()
        if(t.getType() == TokenType.P_OPEN){
            let fc = new NodeFcall();
            if(this.tokens[0].getType() == TokenType.ID){
                fc.fnName = new NodeID(this.tokens[0].getLexema());
                this.tokens.shift();
            }else{
                throw "Error => expected ID after P_OPEN";
            }
            while(this.tokens[0].getType() != TokenType.P_CLOSE){
                fc.params.push(this.parseIntern())
            }
            this.tokens.shift();
            return fc;
        }
        if(t.getType() ==TokenType.ID){
            return new NodeID(t.getLexema());
        }
        if(t.getType() ==TokenType.NUMBER){
            return new NodeNumber(t.getLexema());
        }

    }
    public execute(){
        return this.parseTree.execute();
    }
}

let program = "(+ 5 (- 2 (/ 6 (* 2 1))))" 
let li = new LispInterprete();
li.tokenize(program);
li.parse();
console.log(program+"="+li.execute())

