interface Iterator<T> {
    next(): IteratorResult<T>;
}

interface IteratorResult<T> {
    done: boolean;
    value: T;
}

class HashmapIterator<T> implements Iterator<T>{
    private h:Hashmap<T>;
    private idxBucket = 0;
    private idxElem = 0;
    constructor(h:Hashmap<T>){
        this.h=h;
    }
    next():IteratorResult<T>{
        if(this.idxBucket>=this.h.getSize())
            return {done:true, value:null};
        var b = this.h.getBucket(this.idxBucket);
        if(this.idxElem>=b.length){
            this.idxElem=0;
            this.idxBucket+=1;
            return this.next();
        }
        return {done:false, value:b[this.idxElem++].getKey()};
    }
}

class HashmapElement<T>{
    private s:T;
    private e:any;
    constructor(s:T, e:any){
        this.s=s;
        this.e=e;
    }
    public getKey():T{
        return this.s;
    }
    public getValue():any{
        return this.e;
    }
    public setValue(e:any){
        this.e=e;
    }
}
class Hashmap<T>{
    private hashFunc:(T) => number;
    private size:number;
    private array:Array<Array<HashmapElement<T>>>;
    constructor(h:(T) => number, size:number){
        this.hashFunc = h;
        this.size = size;
        let i = 0;
        this.array = []
        while(i<size){
            this.array.push([])
            i++;
        }
    }
    public set(s:T, e:any){
        var line = this.array[this.hashFunc(s)%this.size];
        for(var v in line){
            if(line[v].getKey()==s){
                line[v].setValue(e);
                return;
            }
        }
        line.push(new HashmapElement<T>(s, e));
    }
    public get(s:T):any{
        var line = this.array[this.hashFunc(s)%this.size];
        for(var v in line){
            if(line[v].getKey()==s){
                return line[v].getValue();
            }
        }
        return null;
    }
    public getBucket(bucket:number):Array<HashmapElement<T>>{
        return this.array[bucket];
    }
    public getSize():number{
        return this.size;
    }

    public iterate():HashmapIterator<T>{
        return new HashmapIterator<T>(this);
    }

}

let h = function(s){
    var hash = 0;
    for (var i = 0; i < s.length; i++) {
        var character = s.charCodeAt(i);
        hash = ((hash<<5)-hash)+character;
        hash = hash & hash;
    }
    return hash;
}

var hm = new Hashmap<string>(h, 10);
hm.set("abc", "v")
hm.set("abc", "testabc")
hm.set("abcd", "h")
hm.set("abcd", "abcd")
hm.set("k1", "v1")
hm.set("k2", "v2")

console.log(hm.get("abc"))
console.log(hm.get("abcd"))
console.log(hm.get("unknown"))


console.log("Iterate:")
var iterator = hm.iterate();
var v;
while(!(v = iterator.next()).done){
    console.log(v.value+" => "+ hm.get(v.value))
}