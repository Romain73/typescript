class LinkedListElement<T>{
    public value:T;
    public next:LinkedListElement<T>;
    constructor(n:T){
        this.value = n;
        this.next=null;
    }
}

interface Iterator<T> {
    next(): IteratorResult<T>;
}

interface IteratorResult<T> {
    done: boolean;
    value: T;
}

class LinkedListIterator<T> implements Iterator<T>{
    private curr:LinkedListElement<T>;
    constructor(e:LinkedListElement<T>){
        this.curr=e;
    }
    next():IteratorResult<T>{
        if(this.curr == null){
            return {done:true, value:null};
        }else{
            let v = this.curr.value;
            this.curr = this.curr.next;
            return {done:false, value:v};
        }
    }
}

class LinkedList<T>{
    private head:LinkedListElement<T>;
    private length:number;

    constructor(){
        this.length=0;
        this.head=null;
    }

    public pushBack(n:T){
        if(this.head == null){
            this.head = new LinkedListElement(n);
        }else{
            let h = this.head;
            while(h.next != null){
                h = h.next;
            }
            h.next = new LinkedListElement(n);
        }
        this.length+=1;
    }
    public popFront():T{
        if(this.length == 0){
            throw new RangeError("Empty list");
        }
        let v = this.head.value;
        this.head = this.head.next;
        this.length-=1;
        return v;
    }

    public iterate():LinkedListIterator<T>{
        return new LinkedListIterator<T>(this.head);
    }

    public toString():string{
        return "List of length: "+this.length;
    }
}

let ll = new LinkedList<number>()
ll.pushBack(1)
ll.pushBack(2)
ll.pushBack(3)
ll.pushBack(4)
console.log("Iterate:")
var iterator = ll.iterate();
var v;
while(!(v = iterator.next()).done){
    console.log(v.value)
}
console.log(ll+"")
console.log(ll.popFront())
console.log(ll.popFront())
console.log(ll.popFront())
console.log(ll.popFront())
try{
    console.log(ll.popFront())
}catch(e){
    console.log(e);
}