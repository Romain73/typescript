class Racional{
    private numerador:number;
    private denominador:number;

    private static gcd(a:number, b:number):number{
    	while(b != 0){
    		let t = b;
    		b = a%b;
    		a = t;
    	}
    	return a;
    }

    private reducir(){
    	let gcd = Racional.gcd(this.numerador, this.denominador);
    	this.numerador/=gcd;
    	this.denominador/=gcd;
    }

    public suma(a:Racional):Racional{
    	return new Racional(this.numerador*a.denominador+a.numerador*this.denominador, this.denominador*a.denominador);
    }

    public multiplicacion(a:Racional):Racional{
    	return new Racional(this.numerador*a.numerador, this.denominador*a.denominador);
    }

    public menor(a:Racional):Boolean{
    	return this.numerador * a.denominador < this.denominador * a.numerador;
    }

    public igual(a:Racional):Boolean{
    	return this.numerador * a.denominador == this.denominador * a.numerador;
    }

    public toReal():number{
    	return this.numerador/this.denominador;
    }

    constructor(numerador=0,denominador=1){
    	if(denominador == 0){
    		throw new RangeError("El denominador no puede tener el valor 0.");
    	}
    	this.numerador = numerador;
    	this.denominador = denominador;
    	this.reducir();
    }

    public toString():string{
    	if(this.denominador == 1){
    		return "("+this.numerador+")";
    	}else{
        	return "("+this.numerador+"/"+this.denominador+")";
    	}
    }
}

let a = new Racional()
let b = new Racional(1,3)
let c = new Racional(4,3)
let d = new Racional(3,2)
console.log("a="+a+"="+a.toReal())
console.log("b="+b+"="+b.toReal())
console.log("c="+c)
console.log("d="+d)
let e = c.suma(d)
let f = c.multiplicacion(d)
let g = c.multiplicacion(b)
console.log("e="+c+"+"+d+"="+e)
console.log("f="+c+"*"+d+"="+f)
console.log("g="+c+"*"+b+"="+g)
try{
	let h = new Racional(1,0)	
}catch(e){
	console.log(e);
}
let i = c.menor(a);
let j = b.menor(c);
let k = b.igual(c);
console.log("i="+c+"<"+a+"="+i)
console.log("j="+b+"<"+c+"="+j)
console.log("k="+b+"=="+c+"="+k)
let l = new Racional(2, 6)
console.log("l="+l)
let m = l.igual(b)
console.log("m="+l+"=="+b+"="+m)
let n = l.menor(b)
console.log("n="+l+"<"+b+"="+n)

